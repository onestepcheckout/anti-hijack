This package contains this README.md

Referring to:
https://blog.packagist.com/preventing-dependency-hijacking/
https://medium.com/@alex.birsan/dependency-confusion-4a5d60fec610

The situation in brief is this:
If a hijacker publishes a package to packagist.org using a
fake vendor prefix which is only previously being used in
private repositories, with a higher version, the package
from packagist.org can be installed instead. This can lead
to malicious software entering into a project.

Composer packages always include a vendor prefix, so with
this package, we are avoiding people with malicious intent
to exploit this when it comes to our software.

As of Composer 2.0, repositories are canonical by default,
meaning that if a package is found in a private repository,
composer will ignore packages from packagist.org. However
the use of 2.0 is not too widespread at the moment.


Our software OneStepCheckout for Magento 1 and Magento 2 are
distributed commercially, and are available through our own
private repository, as well as Magento Marketplace's
repository.

Our vendor prefix on those channels are onestepcheckout,
which also is our business name and our website (at
https://www.onestepcheckout.com) domain.

Magento merchants around the world using our software have
millions in sales, and a hijacking of the packagist.org
repository with malicious code matching our package names
can potentially lead hijacking of webshops.

We hope this will be resolved with Composer 2 getting
more popular, but for the time being we see the need for
public package.

OneStepCheckout, February 2021
